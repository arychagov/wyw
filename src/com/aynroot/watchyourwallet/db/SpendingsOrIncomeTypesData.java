package com.aynroot.watchyourwallet.db;

import java.util.ArrayList;
import java.util.List;

import com.aynroot.watchyourwallet.currency.SpendingsOrIncomeType;
import com.aynroot.watchyourwallet.db.DatabaseHelper;
import net.sqlcipher.database.SQLiteDatabase;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class SpendingsOrIncomeTypesData {
    // Database fields
    private final DatabaseHelper databaseHelper;
    private String m_table;

    public SpendingsOrIncomeTypesData(Context context, String table) {
        DatabaseHelper.init(context);
        databaseHelper = DatabaseHelper.sharedInstance();
        m_table = table;
    }

    public SpendingsOrIncomeType createSpendingsOrIncomeType(String name) {
        final SQLiteDatabase database = databaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.SpendingTypesTableInfo.SpendingTypesTableEntry.COLUMN_NAME, name);
        long insertId = database.insert(m_table, null, values);
        Cursor cursor = database.query(m_table, null,
                DatabaseHelper.SpendingTypesTableInfo.SpendingTypesTableEntry.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        SpendingsOrIncomeType newSpendingsType = cursorToSpendingsType(cursor);
        cursor.close();
        return newSpendingsType;
    }

    public void deleteSpendingsOrIncomeType(SpendingsOrIncomeType spendingsOrIncomeType) {
        final SQLiteDatabase database = databaseHelper.getWritableDatabase();

        long id = spendingsOrIncomeType.getId();
        System.out.println("SpendingsType deleted with id: " + id);
        database.delete(m_table, DatabaseHelper.SpendingTypesTableInfo.SpendingTypesTableEntry.COLUMN_ID + " = " + id, null);
    }

    public String getNameById(long id) {
        final SQLiteDatabase database = databaseHelper.getReadableDatabase();

        Cursor cursor = database.query(m_table, new String[]{DatabaseHelper.SpendingTypesTableInfo.SpendingTypesTableEntry.COLUMN_NAME},
                DatabaseHelper.SpendingTypesTableInfo.SpendingTypesTableEntry.COLUMN_ID + " = " + id,
                null, null, null, null);
        cursor.moveToFirst();
        String result = cursor.getString(0);
        cursor.close();
        return result;
    }

    public List<SpendingsOrIncomeType> getAllSpendingsOrIncomeTypes() {
        final SQLiteDatabase database = databaseHelper.getReadableDatabase();

        List<SpendingsOrIncomeType> spendingsTypes = new ArrayList<SpendingsOrIncomeType>();

        Cursor cursor = database.query(m_table, null, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            SpendingsOrIncomeType spendingsType = cursorToSpendingsType(cursor);
            spendingsTypes.add(spendingsType);
            cursor.moveToNext();
        }
        cursor.close();
        return spendingsTypes;
    }

    private SpendingsOrIncomeType cursorToSpendingsType(Cursor cursor) {
        SpendingsOrIncomeType spendingsType = new SpendingsOrIncomeType();
        spendingsType.setId(cursor.getLong(0));
        spendingsType.setName(cursor.getString(1));
        return spendingsType;
    }

    public void deleteTable() {
        final SQLiteDatabase database = databaseHelper.getWritableDatabase();
        database.delete(m_table, null, null);
    }
}

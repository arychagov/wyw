package com.aynroot.watchyourwallet.db;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;
import android.content.Context;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "main.db";
    private static final int DATABASE_VERSION = 11;
    private static DatabaseHelper instance;

    public SQLiteDatabase getReadableDatabase() {
        return super.getReadableDatabase("accounts");
    }


    public SQLiteDatabase getWritableDatabase() {
        return super.getWritableDatabase("accounts");
    }

    public interface TransactionsCommonColumns {
        public static final String COLUMN_ID = "_id";
        public static final String COLUMN_COMMENT = "comment";
        public static final String COLUMN_CURRENCY = "currency";
        public static final String COLUMN_VALUE = "value";
        public static final String COLUMN_FROM = "from_id";
        public static final String COLUMN_TO = "to_id";
        public static final String COLUMN_DATE = "ts";
        public static final String COLUMN_DONE = "is_done";
    }


    public static interface SpendingsOrIncomeCommonColumns {
        public static final String COLUMN_ID = "_id";
        public static final String COLUMN_NAME = "name";
    }


    public static abstract class AccountTableInfo {

        public static final class AccountTableEntry {
            private AccountTableEntry() {

            }

            public static final String TABLE_ACCOUNTS = "AccountsTable";
            public static final String COLUMN_ID = "_id";
            public static final String COLUMN_NAME = "name";
            public static final String COLUMN_CURRENCY = "currency";
            public static final String COLUMN_BALANCE = "balance";
        }

        public static final String CREATE_TABLE = "create table "
                + AccountTableEntry.TABLE_ACCOUNTS + "("
                + AccountTableEntry.COLUMN_ID + " integer primary key autoincrement, "
                + AccountTableEntry.COLUMN_NAME + " text not null, "
                + AccountTableEntry.COLUMN_CURRENCY + " text not null, "
                + AccountTableEntry.COLUMN_BALANCE + " real);";
    }


    public static abstract class TransactionsSpendingsTableInfo {
        public static final class TransactionsSpendingsTableEntry implements TransactionsCommonColumns {
            private TransactionsSpendingsTableEntry() {
            }

            public static final String TABLE_TRANSACTIONS_SPENDINGS = "TransactionsSpendingsTable";
        }


        private static final String CREATE_TABLE = "create table "
                + TransactionsSpendingsTableEntry.TABLE_TRANSACTIONS_SPENDINGS + "("
                + TransactionsSpendingsTableEntry.COLUMN_ID + " integer primary key autoincrement, "
                + TransactionsSpendingsTableEntry.COLUMN_COMMENT + " text, "
                + TransactionsSpendingsTableEntry.COLUMN_CURRENCY + " text not null, "
                + TransactionsSpendingsTableEntry.COLUMN_VALUE + " real, "
                + TransactionsSpendingsTableEntry.COLUMN_FROM + " integer, "
                + TransactionsSpendingsTableEntry.COLUMN_TO + " integer, "
                + TransactionsSpendingsTableEntry.COLUMN_DATE + " integer, "
                + TransactionsSpendingsTableEntry.COLUMN_DONE + " integer default 0, "
                + " foreign key (" + TransactionsSpendingsTableEntry.COLUMN_FROM + ") references " + AccountTableInfo.AccountTableEntry.TABLE_ACCOUNTS + "(" + AccountTableInfo.AccountTableEntry.COLUMN_ID + "), "
                + " foreign key (" + TransactionsSpendingsTableEntry.COLUMN_TO + ") references " + SpendingTypesTableInfo.SpendingTypesTableEntry.TABLE_SPENDINGS_TYPES + "(" + SpendingTypesTableInfo.SpendingTypesTableEntry.COLUMN_ID + "));";
    }


    public static abstract class TransactionsIncomeTableInfo {
        public static final class TransactionsIncomeTableEntry implements TransactionsCommonColumns {
            private TransactionsIncomeTableEntry() {
            }

            public static final String TABLE_TRANSACTIONS_INCOME = "TransactionsIncomeTable";
        }


        private static final String CREATE_TABLE = "create table "
                + TransactionsIncomeTableEntry.TABLE_TRANSACTIONS_INCOME + "("
                + TransactionsIncomeTableEntry.COLUMN_ID + " integer primary key autoincrement, "
                + TransactionsIncomeTableEntry.COLUMN_COMMENT + " text, "
                + TransactionsIncomeTableEntry.COLUMN_CURRENCY + " text not null, "
                + TransactionsIncomeTableEntry.COLUMN_VALUE + " real, "
                + TransactionsIncomeTableEntry.COLUMN_FROM + " integer, "
                + TransactionsIncomeTableEntry.COLUMN_TO + " integer, "
                + TransactionsIncomeTableEntry.COLUMN_DATE + " integer, "
                + TransactionsIncomeTableEntry.COLUMN_DONE + " integer default 0, "
                + " foreign key (" + TransactionsIncomeTableEntry.COLUMN_FROM + ") references " + IncomeTypesTableInfo.IncomeTypesTableEntry.TABLE_INCOME_TYPES + "(" + IncomeTypesTableInfo.IncomeTypesTableEntry.COLUMN_ID + "), "
                + " foreign key (" + TransactionsIncomeTableEntry.COLUMN_TO + ") references " + AccountTableInfo.AccountTableEntry.TABLE_ACCOUNTS + "(" + AccountTableInfo.AccountTableEntry.COLUMN_ID + "));";
    }


    public static abstract class TransactionsTransferTableInfo {
        public static final class TransactionsTransferTableEntry implements TransactionsCommonColumns {
            private TransactionsTransferTableEntry() {
            }

            public static final String TABLE_TRANSACTIONS_TRANSFER = "TransactionsTransferTable";
        }


        private static final String CREATE_TABLE = "create table "
                + TransactionsTransferTableEntry.TABLE_TRANSACTIONS_TRANSFER + "("
                + TransactionsTransferTableEntry.COLUMN_ID + " integer primary key autoincrement, "
                + TransactionsTransferTableEntry.COLUMN_COMMENT + " text, "
                + TransactionsTransferTableEntry.COLUMN_CURRENCY + " text not null, "
                + TransactionsTransferTableEntry.COLUMN_VALUE + " real, "
                + TransactionsTransferTableEntry.COLUMN_FROM + " integer, "
                + TransactionsTransferTableEntry.COLUMN_TO + " integer, "
                + TransactionsTransferTableEntry.COLUMN_DATE + " integer, "
                + TransactionsTransferTableEntry.COLUMN_DONE + " integer default 0, "
                + " foreign key (" + TransactionsTransferTableEntry.COLUMN_FROM + ") references " + AccountTableInfo.AccountTableEntry.TABLE_ACCOUNTS + "(" + AccountTableInfo.AccountTableEntry.COLUMN_ID + "), "
                + " foreign key (" + TransactionsTransferTableEntry.COLUMN_TO + ") references " + AccountTableInfo.AccountTableEntry.TABLE_ACCOUNTS + "(" + AccountTableInfo.AccountTableEntry.COLUMN_ID + "));";
    }

    public static abstract class SpendingTypesTableInfo {
        public static final class SpendingTypesTableEntry implements SpendingsOrIncomeCommonColumns {
            private SpendingTypesTableEntry() {
            }

            public static final String TABLE_SPENDINGS_TYPES = "SpendingsTypeTable";
        }

        private static final String CREATE_TABLE = "create table "
                + SpendingTypesTableEntry.TABLE_SPENDINGS_TYPES + "("
                + SpendingTypesTableEntry.COLUMN_ID + " integer primary key autoincrement, "
                + SpendingTypesTableEntry.COLUMN_NAME + " text not null);";
    }


    public static abstract class IncomeTypesTableInfo {
        public static final class IncomeTypesTableEntry implements SpendingsOrIncomeCommonColumns {
            private IncomeTypesTableEntry() {
            }

            public static final String TABLE_INCOME_TYPES = "IncomeTypeTable";
        }

        private static final String CREATE_TABLE = "create table "
                + IncomeTypesTableEntry.TABLE_INCOME_TYPES + "("
                + IncomeTypesTableEntry.COLUMN_ID + " integer primary key autoincrement, "
                + IncomeTypesTableEntry.COLUMN_NAME + " text not null);";
    }

    public static DatabaseHelper sharedInstance() {
        if (instance == null) {
            throw new IllegalStateException("Use init(Context) method before using database!");
        }

        return instance;
    }

    public static void init(Context context) {
        getInstance(context);
    }

    private static DatabaseHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseHelper(context);
        }

        return instance;
    }


    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL("pragma foreign_keys = on;");
        database.execSQL(AccountTableInfo.CREATE_TABLE);
        database.execSQL(SpendingTypesTableInfo.CREATE_TABLE);
        database.execSQL(IncomeTypesTableInfo.CREATE_TABLE);
        database.execSQL(TransactionsSpendingsTableInfo.CREATE_TABLE);
        database.execSQL(TransactionsIncomeTableInfo.CREATE_TABLE);
        database.execSQL(TransactionsTransferTableInfo.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(DatabaseHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("drop table if exists " + AccountTableInfo.AccountTableEntry.TABLE_ACCOUNTS);
        db.execSQL("drop table if exists " + SpendingTypesTableInfo.SpendingTypesTableEntry.TABLE_SPENDINGS_TYPES);
        db.execSQL("drop table if exists " + IncomeTypesTableInfo.IncomeTypesTableEntry.TABLE_INCOME_TYPES);
        db.execSQL("drop table if exists " + TransactionsSpendingsTableInfo.TransactionsSpendingsTableEntry.TABLE_TRANSACTIONS_SPENDINGS);
        db.execSQL("drop table if exists " + TransactionsIncomeTableInfo.TransactionsIncomeTableEntry.TABLE_TRANSACTIONS_INCOME);
        db.execSQL("drop table if exists " + TransactionsTransferTableInfo.TransactionsTransferTableEntry.TABLE_TRANSACTIONS_TRANSFER);
        onCreate(db);
    }
}

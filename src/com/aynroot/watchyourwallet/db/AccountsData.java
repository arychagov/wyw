package com.aynroot.watchyourwallet.db;

import java.util.ArrayList;
import java.util.List;

import com.aynroot.watchyourwallet.currency.Account;
import com.aynroot.watchyourwallet.currency.Currency;
import net.sqlcipher.database.SQLiteDatabase;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

public class AccountsData {
    // Database fields
    private DatabaseHelper dbHelper;

    public AccountsData(Context context) {
        DatabaseHelper.init(context);
        dbHelper = DatabaseHelper.sharedInstance();
    }

    public Account createAccount(String name, String currency, float balance) {
        final SQLiteDatabase database = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.AccountTableInfo.AccountTableEntry.COLUMN_NAME, name);
        values.put(DatabaseHelper.AccountTableInfo.AccountTableEntry.COLUMN_CURRENCY, currency);
        values.put(DatabaseHelper.AccountTableInfo.AccountTableEntry.COLUMN_BALANCE, balance);
        long insertId = database.insert(DatabaseHelper.AccountTableInfo.AccountTableEntry.TABLE_ACCOUNTS, null, values);
        Cursor cursor = database.query(DatabaseHelper.AccountTableInfo.AccountTableEntry.TABLE_ACCOUNTS, null,
                DatabaseHelper.AccountTableInfo.AccountTableEntry.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Account newAccount = cursorToAccount(cursor);
        cursor.close();
        return newAccount;
    }

    public void deleteAccount(Account account) {
        final SQLiteDatabase database = dbHelper.getWritableDatabase();
        long id = account.getId();
        System.out.println("Account deleted with id: " + id);
        database.delete(DatabaseHelper.AccountTableInfo.AccountTableEntry.TABLE_ACCOUNTS, DatabaseHelper.AccountTableInfo.AccountTableEntry.COLUMN_ID + " = " + id, null);
    }

    public List<Account> getAllAccounts() {
        final SQLiteDatabase database = dbHelper.getReadableDatabase();

        List<Account> accounts = new ArrayList<Account>();

        Cursor cursor = database.query(DatabaseHelper.AccountTableInfo.AccountTableEntry.TABLE_ACCOUNTS,
                null, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Account account = cursorToAccount(cursor);
            accounts.add(account);
            Log.v("test", String.valueOf(account.getId()));
            cursor.moveToNext();
        }
        cursor.close();
        return accounts;
    }


    public float getCurBalance(long id) {
        final SQLiteDatabase database = dbHelper.getReadableDatabase();

        Cursor cursor = database.query(DatabaseHelper.AccountTableInfo.AccountTableEntry.TABLE_ACCOUNTS,
                new String[]{DatabaseHelper.AccountTableInfo.AccountTableEntry.COLUMN_BALANCE},
                DatabaseHelper.AccountTableInfo.AccountTableEntry.COLUMN_ID + " = ?",
                new String[]{Long.toString(id)}, null, null, null);
        if (cursor.getCount() < 1) {
            cursor.close();
            throw new IllegalStateException();
        }
        cursor.moveToFirst();
        float result = cursor.getFloat(0);
        cursor.close();
        return result;
    }

    public void updateAccountBalance(long id, float value) {
        final SQLiteDatabase database = dbHelper.getWritableDatabase();
        float curBalance = getCurBalance(id);

        ContentValues args = new ContentValues();
        args.put(DatabaseHelper.AccountTableInfo.AccountTableEntry.COLUMN_BALANCE, curBalance + value);
        database.update(DatabaseHelper.AccountTableInfo.AccountTableEntry.TABLE_ACCOUNTS, args, DatabaseHelper.AccountTableInfo.AccountTableEntry.COLUMN_ID + "=" + id, null);
    }

    private Account cursorToAccount(Cursor cursor) {
        Account account = new Account();
        account.setId(cursor.getLong(0));
        account.setName(cursor.getString(1));
        account.setCurrency(Currency.valueOf(cursor.getString(2)));
        account.setBalance(cursor.getFloat(3));
        return account;
    }

    public void deleteTable() {
        final SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(DatabaseHelper.AccountTableInfo.AccountTableEntry.TABLE_ACCOUNTS, null, null);
    }
} 
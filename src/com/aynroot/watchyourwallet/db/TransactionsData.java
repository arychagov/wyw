package com.aynroot.watchyourwallet.db;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.aynroot.watchyourwallet.currency.Currency;
import com.aynroot.watchyourwallet.currency.Transaction;
import com.aynroot.watchyourwallet.currency.TransactionType;
import com.aynroot.watchyourwallet.db.DatabaseHelper;
import net.sqlcipher.database.SQLiteDatabase;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class TransactionsData {
    private DatabaseHelper dbHelper;

    public TransactionsData(Context context) {
        DatabaseHelper.init(context);
        dbHelper = DatabaseHelper.sharedInstance();
    }

    private String getReferenceTable(String tTypeStr) {
        TransactionType tType = TransactionType.valueOf(tTypeStr);
        String refTable = null;
        switch (tType) {
            case SPENDING:
                refTable = DatabaseHelper.TransactionsSpendingsTableInfo.TransactionsSpendingsTableEntry.TABLE_TRANSACTIONS_SPENDINGS;
                break;
            case INCOME:
                refTable = DatabaseHelper.TransactionsIncomeTableInfo.TransactionsIncomeTableEntry.TABLE_TRANSACTIONS_INCOME;
                break;
            case TRANSFER:
                refTable = DatabaseHelper.TransactionsTransferTableInfo.TransactionsTransferTableEntry.TABLE_TRANSACTIONS_TRANSFER;
                break;
        }
        return refTable;
    }

    public Transaction createTransaction(String comment, String currency, float value,
                                         int fromId, int toId, String dateStr, String tType) throws ParseException {
        final SQLiteDatabase database = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.TransactionsCommonColumns.COLUMN_COMMENT, comment);
        values.put(DatabaseHelper.TransactionsCommonColumns.COLUMN_CURRENCY, currency);
        values.put(DatabaseHelper.TransactionsCommonColumns.COLUMN_VALUE, value);
        values.put(DatabaseHelper.TransactionsCommonColumns.COLUMN_FROM, fromId);
        values.put(DatabaseHelper.TransactionsCommonColumns.COLUMN_TO, toId);
        values.put(DatabaseHelper.TransactionsCommonColumns.COLUMN_DATE, Transaction.unixTsFromDate(dateStr));

        String refTable = getReferenceTable(tType);
        long insertId = database.insert(refTable, null, values);
        Cursor cursor = database.query(refTable, null, DatabaseHelper.TransactionsCommonColumns.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Transaction newTransaction = cursorToTransaction(cursor, tType);
        cursor.close();
        return newTransaction;
    }

    public void deleteTransaction(Transaction transaction) {
        final SQLiteDatabase database = dbHelper.getWritableDatabase();
        long id = transaction.getId();
        String refTable = getReferenceTable(transaction.getType());
        System.out.println("Transaction deleted with id: " + id);
        database.delete(refTable, DatabaseHelper.TransactionsCommonColumns.COLUMN_ID + " = " + id, null);
    }

    public void updateTransactionState(String tType, long id) {
        final SQLiteDatabase database = dbHelper.getWritableDatabase();
        ContentValues args = new ContentValues();
        args.put(DatabaseHelper.TransactionsCommonColumns.COLUMN_DONE, 1);
        String refTable = getReferenceTable(tType);
        database.update(refTable, args, DatabaseHelper.TransactionsCommonColumns.COLUMN_ID + "=" + id, null);
    }

    public List<Transaction> getTransactionsFromCursor(Cursor cursor, String tType) {
        List<Transaction> transactions = new ArrayList<Transaction>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Transaction transaction = cursorToTransaction(cursor, tType);
            transactions.add(transaction);
            cursor.moveToNext();
        }
        return transactions;
    }

    private List<Transaction> mergeCursorsToTransactions(Cursor cursor1, Cursor cursor2, Cursor cursor3) {
        List<Transaction> list1 = getTransactionsFromCursor(cursor1, (TransactionType.SPENDING).name());
        List<Transaction> list2 = getTransactionsFromCursor(cursor2, (TransactionType.INCOME).name());
        List<Transaction> list3 = getTransactionsFromCursor(cursor3, (TransactionType.TRANSFER).name());
        cursor1.close();
        cursor2.close();
        cursor3.close();

        List<Transaction> transactions = new ArrayList<Transaction>();

        transactions.addAll(list1);
        transactions.addAll(list2);
        transactions.addAll(list3);
        return transactions;
    }

    public List<Transaction> getAllPreviousTransactions() {
        final SQLiteDatabase database = dbHelper.getReadableDatabase();

        Cursor cursor1 = database.query(DatabaseHelper.TransactionsSpendingsTableInfo.TransactionsSpendingsTableEntry.TABLE_TRANSACTIONS_SPENDINGS, null,
                DatabaseHelper.TransactionsCommonColumns.COLUMN_DONE + " = 1", null, null, null, null);
        Cursor cursor2 = database.query(DatabaseHelper.TransactionsIncomeTableInfo.TransactionsIncomeTableEntry.TABLE_TRANSACTIONS_INCOME, null,
                DatabaseHelper.TransactionsCommonColumns.COLUMN_DONE + " = 1", null, null, null, null);
        Cursor cursor3 = database.query(DatabaseHelper.TransactionsTransferTableInfo.TransactionsTransferTableEntry.TABLE_TRANSACTIONS_TRANSFER, null,
                DatabaseHelper.TransactionsCommonColumns.COLUMN_DONE + " = 1", null, null, null, null);

        List<Transaction> transactions = mergeCursorsToTransactions(cursor1, cursor2, cursor3);
        Collections.sort(transactions, Collections.reverseOrder());
        return transactions;
    }

    public List<Transaction> getAllPlannedTransactions() {
        final SQLiteDatabase database = dbHelper.getReadableDatabase();

        Date d = new Date();
        long today = d.getTime() / 1000L;
        Cursor cursor1 = database.query(DatabaseHelper.TransactionsSpendingsTableInfo.TransactionsSpendingsTableEntry.TABLE_TRANSACTIONS_SPENDINGS, null,
                "(" + DatabaseHelper.TransactionsCommonColumns.COLUMN_DONE + " = 0 and " + DatabaseHelper.TransactionsCommonColumns.COLUMN_DATE + " <= " + today +
                        ") or (" + DatabaseHelper.TransactionsCommonColumns.COLUMN_DONE + " = 2 and " +
                        DatabaseHelper.TransactionsCommonColumns.COLUMN_DATE + " < " + today + ")", null, null, null, null);
        Cursor cursor2 = database.query(DatabaseHelper.TransactionsIncomeTableInfo.TransactionsIncomeTableEntry.TABLE_TRANSACTIONS_INCOME, null,
                "(" + DatabaseHelper.TransactionsCommonColumns.COLUMN_DONE + " = 0 and " + DatabaseHelper.TransactionsCommonColumns.COLUMN_DATE + " <= " + today +
                        ") or (" + DatabaseHelper.TransactionsCommonColumns.COLUMN_DONE + " = 2 and " +
                        DatabaseHelper.TransactionsCommonColumns.COLUMN_DATE + " < " + today + ")", null, null, null, null);
        Cursor cursor3 = database.query(DatabaseHelper.TransactionsTransferTableInfo.TransactionsTransferTableEntry.TABLE_TRANSACTIONS_TRANSFER, null,
                "(" + DatabaseHelper.TransactionsCommonColumns.COLUMN_DONE + " = 0 and " + DatabaseHelper.TransactionsCommonColumns.COLUMN_DATE + " <= " + today +
                        ") or (" + DatabaseHelper.TransactionsCommonColumns.COLUMN_DONE + " = 2 and " +
                        DatabaseHelper.TransactionsCommonColumns.COLUMN_DATE + " < " + today + ")", null, null, null, null);

        List<Transaction> transactions = mergeCursorsToTransactions(cursor1, cursor2, cursor3);
        Collections.sort(transactions, Collections.reverseOrder());
        return transactions;

    }

    public List<Transaction> getAllTransactions(String tType) {
        final SQLiteDatabase database = dbHelper.getReadableDatabase();

        List<Transaction> transactions = new ArrayList<Transaction>();

        String refTable = getReferenceTable(tType);
        Cursor cursor = database.query(refTable, null, null, null, null, null, DatabaseHelper.TransactionsCommonColumns.COLUMN_DATE + " desc");

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Transaction transaction = cursorToTransaction(cursor, tType);
            transactions.add(transaction);
            cursor.moveToNext();
        }
        cursor.close();
        return transactions;
    }

    public List<Transaction> getAllAccountTransactions(long id) {
        final SQLiteDatabase database = dbHelper.getReadableDatabase();

        Cursor cursor1 = database.query(DatabaseHelper.TransactionsSpendingsTableInfo.TransactionsSpendingsTableEntry.TABLE_TRANSACTIONS_SPENDINGS, null,
                DatabaseHelper.TransactionsCommonColumns.COLUMN_DONE + " = 1 and " + DatabaseHelper.TransactionsCommonColumns.COLUMN_FROM + " = " + id, null, null, null, null);
        Cursor cursor2 = database.query(DatabaseHelper.TransactionsIncomeTableInfo.TransactionsIncomeTableEntry.TABLE_TRANSACTIONS_INCOME, null,
                DatabaseHelper.TransactionsCommonColumns.COLUMN_DONE + " = 1 and " + DatabaseHelper.TransactionsCommonColumns.COLUMN_TO + " = " + id, null, null, null, null);
        Cursor cursor3 = database.query(DatabaseHelper.TransactionsTransferTableInfo.TransactionsTransferTableEntry.TABLE_TRANSACTIONS_TRANSFER, null,
                DatabaseHelper.TransactionsCommonColumns.COLUMN_DONE + " = 1 and (" + DatabaseHelper.TransactionsCommonColumns.COLUMN_FROM + " = " + id
                        + " or " + DatabaseHelper.TransactionsCommonColumns.COLUMN_TO + " = " + id + ")", null, null, null, null);

        List<Transaction> transactions = mergeCursorsToTransactions(cursor1, cursor2, cursor3);
        Collections.sort(transactions, Collections.reverseOrder());
        return transactions;
    }

    public List<Transaction> getSpendingsTransactions(long id, long startTs, long stopTs) {
        final SQLiteDatabase database = dbHelper.getReadableDatabase();

        if (stopTs == 0) {
            Date d = new Date();
            stopTs = d.getTime() / 1000L;
        }
        Cursor cursor = database.query(DatabaseHelper.TransactionsSpendingsTableInfo.TransactionsSpendingsTableEntry.TABLE_TRANSACTIONS_SPENDINGS, null,
                DatabaseHelper.TransactionsCommonColumns.COLUMN_FROM + " = " + id + " and " +
                        DatabaseHelper.TransactionsCommonColumns.COLUMN_DONE + " = 1 and " +
                        DatabaseHelper.TransactionsCommonColumns.COLUMN_DATE + " >= " + String.valueOf(startTs) + " and " +
                        DatabaseHelper.TransactionsCommonColumns.COLUMN_DATE + " <= " + String.valueOf(stopTs), null, null, null, null);
        List<Transaction> transactions = new ArrayList<Transaction>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Transaction transaction = cursorToTransaction(cursor, (TransactionType.SPENDING).name());
            transactions.add(transaction);
            cursor.moveToNext();
        }
        cursor.close();
        return transactions;
    }

    public void postponeTransaction(Transaction transaction) {
        changeTransactionDoneState(transaction, 2);
    }

    public void completeTransaction(Transaction transaction) {
        changeTransactionDoneState(transaction, 1);
    }

    public void changeTransactionDoneState(Transaction transaction, int newState) {
        final SQLiteDatabase database = dbHelper.getWritableDatabase();

        ContentValues args = new ContentValues();
        args.put(DatabaseHelper.TransactionsCommonColumns.COLUMN_DONE, newState);
        String refTable = getReferenceTable(transaction.getType());
        database.update(refTable, args, DatabaseHelper.TransactionsCommonColumns.COLUMN_ID + "=" + transaction.getId(), null);
    }

    private Transaction cursorToTransaction(Cursor cursor, String tType) {
        Transaction transaction = new Transaction();
        transaction.setId(cursor.getLong(0));
        transaction.setComment(cursor.getString(1));
        transaction.setCurrency(Currency.valueOf(cursor.getString(2)));
        transaction.setValue(cursor.getFloat(3));
        transaction.setFromId(cursor.getInt(4));
        transaction.setToId(cursor.getInt(5));
        transaction.setDate(Transaction.dateFromUnixTs(cursor.getInt(6)));
        transaction.setType(tType);
        return transaction;
    }

    public void deleteTables() {
        final SQLiteDatabase database = dbHelper.getWritableDatabase();

        database.delete(DatabaseHelper.TransactionsIncomeTableInfo.TransactionsIncomeTableEntry.TABLE_TRANSACTIONS_INCOME, null, null);
        database.delete(DatabaseHelper.TransactionsSpendingsTableInfo.TransactionsSpendingsTableEntry.TABLE_TRANSACTIONS_SPENDINGS, null, null);
        database.delete(DatabaseHelper.TransactionsTransferTableInfo.TransactionsTransferTableEntry.TABLE_TRANSACTIONS_TRANSFER, null, null);
    }
}

package com.aynroot.watchyourwallet.items;

public class EntryItem implements Item {

    public String title;
    public String subtitle;

    @Override
    public boolean isSection() {
        return false;
    }

    public String getIconName() {
        return null;
    }
}

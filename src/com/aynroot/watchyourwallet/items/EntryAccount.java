package com.aynroot.watchyourwallet.items;


import com.aynroot.watchyourwallet.currency.Account;

public class EntryAccount extends EntryItem {

    private final Account account;

    public EntryAccount(String title, String subtitle, Account account) {
        this.title = title;
        this.subtitle = subtitle;
        this.account = account;
    }

    @Override
    public String getIconName() {
        return "ic_launcher";
    }

    public Account getAccount() {
        return account;
    }
}

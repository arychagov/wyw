package com.aynroot.watchyourwallet.items;


import com.aynroot.watchyourwallet.currency.Transaction;
import com.aynroot.watchyourwallet.currency.TransactionType;

public class EntryTransaction extends EntryItem {

    private final Transaction transaction;

    public EntryTransaction(String title, String subtitle, Transaction transaction) {
        this.title = title;
        this.subtitle = subtitle;
        this.transaction = transaction;
    }

    @Override
    public String getIconName() {
        TransactionType tType = TransactionType.valueOf(transaction.getType());
        switch (tType) {
            case SPENDING:
                return "ic_minus";
            case INCOME:
                return "ic_plus";
            case TRANSFER:
                return "ic_plus_minus";
        }
        return null;
    }

    public Transaction getTransaction() {
        return transaction;
    }

}

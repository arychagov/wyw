package com.aynroot.watchyourwallet.fragments;

import java.lang.reflect.Field;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.DatePicker;
import com.aynroot.watchyourwallet.dialogs.PermanentTitleDatePickerDialog;

@SuppressLint("ValidFragment")
public class MonthYearPickerDialogFragment extends DialogFragment {
    private OnDateSetListener mDateSetListener;

    public MonthYearPickerDialogFragment() {
    }

    public MonthYearPickerDialogFragment(OnDateSetListener callback) {
        mDateSetListener = callback;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {

        return this.customDatePicker();
    }

    private DatePickerDialog customDatePicker() {
        Calendar cal = Calendar.getInstance();
        PermanentTitleDatePickerDialog dpd = new PermanentTitleDatePickerDialog(this.getActivity(), mDateSetListener, cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
        dpd.setPermanentTitle("pick a date");

        try {
            Field[] datePickerDialogFields = dpd.getClass().getSuperclass().getDeclaredFields();
            for (Field datePickerDialogField : datePickerDialogFields) {
                if (datePickerDialogField.getName().equals("mDatePicker")) {
                    datePickerDialogField.setAccessible(true);
                    DatePicker datePicker = (DatePicker) datePickerDialogField
                            .get(dpd);
                    Field datePickerFields[] = datePickerDialogField.getType()
                            .getDeclaredFields();
                    for (Field datePickerField : datePickerFields) {
                        if ("mDayPicker".equals(datePickerField.getName())
                                || "mDaySpinner".equals(datePickerField
                                .getName())) {
                            datePickerField.setAccessible(true);
                            Object dayPicker = datePickerField.get(datePicker);
                            ((View) dayPicker).setVisibility(View.GONE);
                        }
                    }
                }

            }
        } catch (Exception ex) {
        }
        return dpd;
    }
}

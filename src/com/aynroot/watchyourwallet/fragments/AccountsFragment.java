package com.aynroot.watchyourwallet.fragments;

import java.util.ArrayList;
import java.util.List;

import com.aynroot.watchyourwallet.*;
import com.aynroot.watchyourwallet.activities.CreateAccountActivity;
import com.aynroot.watchyourwallet.currency.Account;
import com.aynroot.watchyourwallet.db.AccountsData;
import com.aynroot.watchyourwallet.items.EntryAccount;
import com.aynroot.watchyourwallet.items.EntryAdapter;
import com.aynroot.watchyourwallet.items.Item;
import net.sqlcipher.database.SQLiteDatabase;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragment;

public class AccountsFragment extends SherlockFragment {
    public final static String ACCOUNT_CREATED_MESSAGE = "com.aynroot.watchyourwallet.ACCOUNT_CREATED_MESSAGE";

    private AccountsData dataSource;
    private ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        SQLiteDatabase.loadLibs(getActivity());
        final View v = inflater.inflate(R.layout.fragment_accounts, container, false);

        if (v != null) {
            listView = (ListView) v.findViewById(R.id.accountsList);
            listView.setLongClickable(true);
            listView.setOnItemLongClickListener(new OnItemLongClickListener() {
                public boolean onItemLongClick(AdapterView<?> parent, View v, int position, long id) {
                    final int itemId = (int) id;
                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                    alertDialogBuilder.setTitle(getResources().getString(R.string.delete_account_title));
                    alertDialogBuilder.setMessage(getResources().getString(R.string.are_you_sure_message))
                            .setCancelable(true).setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            deleteRow(itemId);
                            dialog.cancel();
                        }
                    }).setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                    return true;
                }
            });

            dataSource = new AccountsData(getActivity());

            updateAccountsList();
        }
        return v;
    }

    private void updateAccountsList() {
        List<Account> values = dataSource.getAllAccounts();

        final ArrayList<Item> items = new ArrayList<Item>();
        for (final Account a : values) {
            items.add(new EntryAccount(a.getName(), String.valueOf(a.getBalance()) + " " + a.getCurrency(), a));
        }

        final EntryAdapter adapter = new EntryAdapter(getActivity(), items);
        listView.setAdapter(adapter);
    }

    public void addRow(View v) {
        startActivityForResult(new Intent(getActivity(), CreateAccountActivity.class), 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data == null) {
            return;
        }
        String accountContent = data.getStringExtra(ACCOUNT_CREATED_MESSAGE);
        String[] parts = accountContent.split("\t");

        dataSource.createAccount(parts[0], parts[1], Float.parseFloat(parts[2]));

        updateAccountsList();
    }

    public void deleteRow(int id) {
        EntryAdapter lvAdapter = (EntryAdapter) listView.getAdapter();
        Account account = ((EntryAccount) lvAdapter.getItem(id)).getAccount();

        dataSource.deleteAccount(account);

        updateAccountsList();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateAccountsList();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Activity a = getActivity();
            if (a != null)
                a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }
}

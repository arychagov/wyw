package com.aynroot.watchyourwallet.fragments;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.ViewSwitcher;

import com.actionbarsherlock.app.SherlockFragment;
import com.androidplot.Plot;
import com.androidplot.pie.PieChart;
import com.androidplot.pie.PieRenderer;
import com.androidplot.pie.Segment;
import com.androidplot.pie.SegmentFormatter;
import com.androidplot.ui.SizeLayoutType;
import com.androidplot.ui.SizeMetrics;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;
import com.androidplot.xy.XYStepMode;
import com.aynroot.watchyourwallet.*;
import com.aynroot.watchyourwallet.currency.*;
import com.aynroot.watchyourwallet.db.AccountsData;
import com.aynroot.watchyourwallet.db.DatabaseHelper;
import com.aynroot.watchyourwallet.db.SpendingsOrIncomeTypesData;
import com.aynroot.watchyourwallet.db.TransactionsData;

@SuppressLint("SimpleDateFormat")
public class StatsFragment extends SherlockFragment implements OnDateSetListener {

    private XYPlot plot;
    private PieChart pie;
    private Spinner accountsSpinner1, accountsSpinner2;
    private long curAccountId;
    private float minBalance, maxBalance;

    private Handler pieHandler = new Handler();
    private Handler plotHandler = new Handler();

    private UpdatePlotRunnable plotRunnable = new UpdatePlotRunnable();
    private UpdatePieRunnable pieRunnable = new UpdatePieRunnable();
    private ComplexUpdatePieRunnable complexPieRunnable = new ComplexUpdatePieRunnable();

    private int mYear;

    private int mMonth;
    private TransactionsData transactionsDS;
    private AccountsData accountsDS;

    private SpendingsOrIncomeTypesData spendingsDS;
    private ViewSwitcher switcher;

    private ListView lv;
    private DatePicker.OnDateChangedListener onDateSetListener = new DatePicker.OnDateChangedListener() {

        public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            mYear = year;
            mMonth = monthOfYear;
            Log.v("test", "on date changed");
            updatePlot(curAccountId);
            updateChart(curAccountId);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        transactionsDS = new TransactionsData(getActivity());
        accountsDS = new AccountsData(getActivity());
        spendingsDS = new SpendingsOrIncomeTypesData(getActivity(), DatabaseHelper.SpendingTypesTableInfo.SpendingTypesTableEntry.TABLE_SPENDINGS_TYPES);

        Date today = new Date();
        Calendar cal0 = Calendar.getInstance();
        cal0.setTime(today);
        mMonth = cal0.get(Calendar.MONTH);
        mYear = cal0.get(Calendar.YEAR);
    }

    public void switchLayout(View v) {
        switcher.showNext();
    }

    private ArrayAdapter<Account> updateAccountsSpinner(Spinner spinner) {
        List<Account> accountsList = accountsDS.getAllAccounts();

        ArrayAdapter<Account> dataAdapter = new ArrayAdapter<Account>(getActivity(),
                android.R.layout.simple_spinner_item,
                accountsList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        return dataAdapter;
    }

    private float getBalanceDelta(Transaction t, long id) {
        TransactionType tType = TransactionType.valueOf(t.getType());
        float delta = 0.0f;
        switch (tType) {
            case SPENDING:
                delta = t.getValue();
                break;
            case INCOME:
                delta = -t.getValue();
                break;
            case TRANSFER:
                if (t.getFromId() == id)
                    delta = t.getValue();
                else
                    delta = -t.getValue();
                break;
        }
        return delta;
    }

    private int getIncrement() {
        int delta = (int) (maxBalance - minBalance);
        int final_inc = 0;
        int increments[] = {50, 100, 300, 500, 1000, 300, 5000, 10000, 15000, 20000, 50000};
        for (int inc : increments) {
            if (delta / inc <= 10) {
                final_inc = inc;
                break;
            }
        }
        return final_inc;
    }

    private XYSeries getPlottingData(long id, long startTs, long stopTs) throws ParseException {
        ArrayList<Long> x = new ArrayList<Long>();
        ArrayList<Float> y = new ArrayList<Float>();

        List<Transaction> transactions = transactionsDS.getAllAccountTransactions(id);

        float curBalance;
        try {
            curBalance = accountsDS.getCurBalance(id);
        } catch (IllegalStateException e) {
            curBalance = 0.f;
        }

        long prevTs = startTs;
        minBalance = curBalance;
        maxBalance = curBalance;
        for (Transaction t : transactions) {
            if (startTs > Transaction.unixTsFromDate(t.getDate()))
                break;
            long curTs = Transaction.unixTsFromDate(t.getDate());
            if ((Transaction.unixTsFromDate(t.getDate()) != prevTs) &&
                    (stopTs > Transaction.unixTsFromDate(t.getDate()))) {
                x.add(0, Transaction.unixTsFromDate(t.getDate()));
                y.add(0, curBalance);
                prevTs = curTs;
                minBalance = Math.min(curBalance, minBalance);
                maxBalance = Math.max(curBalance, maxBalance);
            }
            curBalance += getBalanceDelta(t, id);
        }
        x.add(0, startTs);
        y.add(0, curBalance);
        minBalance = Math.min(curBalance, minBalance);
        maxBalance = Math.max(curBalance, maxBalance);

        return new SimpleXYSeries(x, y, "");
    }

    private void updateChartsSettings() {
        plot.setBorderStyle(Plot.BorderStyle.NONE, null, null);
        plot.setPlotMargins(0, 0, 0, 0);
        plot.setPlotPadding(0, 0, 0, 0);
        plot.setGridPadding(0, 0, 0, 0);

        pie.setPlotMargins(0, 0, 0, 0);
        pie.setPlotPadding(0, 0, 0, 0);

        //Set paints
        plot.getGraphWidget().getBackgroundPaint().setColor(Color.WHITE);
        plot.getGraphWidget().getGridBackgroundPaint().setColor(Color.WHITE);
        plot.getBackgroundPaint().setColor(Color.WHITE);
        plot.getBorderPaint().setColor(Color.WHITE);
        plot.setBackgroundColor(Color.WHITE);

        plot.getGraphWidget().getDomainLabelPaint().setColor(Color.BLACK);
        plot.getGraphWidget().getRangeLabelPaint().setColor(Color.BLACK);

        plot.getGraphWidget().getDomainOriginLabelPaint().setColor(Color.BLACK);
        plot.getGraphWidget().getDomainOriginLinePaint().setColor(Color.BLACK);
        plot.getGraphWidget().getRangeOriginLinePaint().setColor(Color.BLACK);

        pie.getBackgroundPaint().setColor(Color.WHITE);
        pie.getBorderPaint().setColor(Color.WHITE);
        pie.setBackgroundColor(Color.WHITE);

        pie.getBackgroundPaint().setColor(Color.WHITE);
        pie.getBorderPaint().setColor(Color.WHITE);
        pie.setBackgroundColor(Color.WHITE);


        //Remove legend
        plot.getLayoutManager().remove(plot.getLegendWidget());
        plot.getLayoutManager().remove(plot.getDomainLabelWidget());
        plot.getLayoutManager().remove(plot.getRangeLabelWidget());
        plot.getLayoutManager().remove(plot.getTitleWidget());
        plot.getLayoutManager().setMarkupEnabled(false);

        //Set Margins
        plot.getGraphWidget().setMarginTop(20);
        plot.getGraphWidget().setMarginRight(20);
        plot.getGraphWidget().setMarginBottom(15);
        plot.getGraphWidget().setMarginLeft(35);

        pie.getPieWidget().setMarginTop(5);
        pie.getPieWidget().setMarginRight(0);
        pie.getPieWidget().setMarginBottom(0);
        pie.getPieWidget().setMarginLeft(5);

        // FILL mode with values of 0 means fill 100% of container:
        SizeMetrics sm = new SizeMetrics(0, SizeLayoutType.FILL,
                0, SizeLayoutType.FILL);
        plot.getGraphWidget().setSize(sm);
        pie.getPieWidget().setSize(sm);

        // customize our domain/range labels
        plot.setRangeLabel("balance"); //TODO currency instead

        // get rid of decimal points in our range labels:
        plot.setRangeValueFormat(new DecimalFormat("0"));
        plot.setDomainValueFormat(new Format() {
            private static final long serialVersionUID = 1L;
            private SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM");

            @Override
            public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
                long timestamp = ((Number) obj).longValue() * 1000;
                Date date = new Date(timestamp);
                return dateFormat.format(date, toAppendTo, pos);
            }

            @Override
            public Object parseObject(String source, ParsePosition pos) {
                return null;
            }
        });
    }

    private void updatePlot(long curAccountId) {

        Calendar cal1 = Calendar.getInstance();
        cal1.set(Calendar.MONTH, mMonth);
        cal1.set(Calendar.YEAR, mYear);
        cal1.set(Calendar.DAY_OF_MONTH, 1);
        Date day1 = cal1.getTime();

        cal1.set(Calendar.DAY_OF_MONTH, 31);
        Date day30 = cal1.getTime();

        // create chart
        XYSeries series;
        long startTs, stopTs;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            startTs = Transaction.unixTsFromDate(sdf.format(day1));
            stopTs = Transaction.unixTsFromDate(sdf.format(day30));
            series = getPlottingData(curAccountId, startTs, stopTs);
            plotChart(startTs, stopTs, series);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void updateChart(final long curAccountId) {

        Calendar cal1 = Calendar.getInstance();
        cal1.set(Calendar.MONTH, mMonth);
        cal1.set(Calendar.YEAR, mYear);
        cal1.set(Calendar.DAY_OF_MONTH, 1);
        Date day1 = cal1.getTime();

        cal1.set(Calendar.DAY_OF_MONTH, 31);
        Date day30 = cal1.getTime();

        // create chart
        XYSeries series;
        final long startTs, stopTs;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            startTs = Transaction.unixTsFromDate(sdf.format(day1));
            stopTs = Transaction.unixTsFromDate(sdf.format(day30));


            pieHandler.removeCallbacks(complexPieRunnable);
            complexPieRunnable.setCurrAccountId(curAccountId);
            complexPieRunnable.setStartTs(startTs);
            complexPieRunnable.setStopTs(stopTs);
            pieHandler.post(complexPieRunnable);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void plotChart(long startTs, long stopTs, XYSeries series) {
        plot.clear();
        plot.setRangeBoundaries(((int) minBalance / 50) * 50, ((int) maxBalance / 50 + 1) * 50, BoundaryMode.FIXED);
        plot.setDomainBoundaries(startTs, stopTs, BoundaryMode.FIXED);

        plot.setRangeStep(XYStepMode.INCREMENT_BY_VAL, getIncrement());
        plot.setDomainStep(XYStepMode.INCREMENT_BY_VAL, 7 * 24 * 3600);

        // setup our line fill paint to be a slightly transparent gradient:
        Paint lineFill = new Paint();
        lineFill.setAlpha(200);
        lineFill.setShader(new LinearGradient(0, 0, 0, 250, Color.WHITE, Color.rgb(81, 192, 232), Shader.TileMode.CLAMP));
        LineAndPointFormatter formatter = new LineAndPointFormatter(Color.rgb(108, 158, 0), Color.rgb(64, 112, 0), Color.BLACK, null);
        formatter.setFillPaint(lineFill);

        plot.addSeries(series, formatter);
        plot.redraw();
    }

    private TreeMap<Float, String> getPieData(long id, long startTs, long stopTs) {
        Log.v("test", String.valueOf(startTs) + " " + String.valueOf(stopTs));
        List<Transaction> transactions = transactionsDS.getSpendingsTransactions(id, startTs, stopTs);

        TreeMap<String, Float> result = new TreeMap<String, Float>();
        Float sum = 0.0f;
        for (Transaction t : transactions) {
            String key = spendingsDS.getNameById(t.getToId());
            if (result.containsKey(key)) {
                Float newValue = result.get(key) + t.getValue();
                result.put(key, newValue);
            } else
                result.put(key, t.getValue());
            sum += t.getValue();
        }

        TreeMap<Float, String> percentages = new TreeMap<Float, String>(Collections.reverseOrder());
        for (Entry<String, Float> entry : result.entrySet()) {
            percentages.put(entry.getValue(), new DecimalFormat("#").format(entry.getValue() * 100 / sum) + "%: " + entry.getKey());
        }
        Log.v("test", String.valueOf(percentages.size()));
        return percentages;
    }

    private TreeMap<Float, String> getPieData(long id) throws ParseException {

        Calendar cal1 = Calendar.getInstance();
        cal1.set(Calendar.MONTH, mMonth);
        cal1.set(Calendar.YEAR, mYear);
        cal1.set(Calendar.DAY_OF_MONTH, 1);
        Date day1 = cal1.getTime();

        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        return getPieData(id, Transaction.unixTsFromDate(sdf.format(day1)), 0);
    }


    private void drawPie(TreeMap<Float, String> data) {
        pie.clear();
        int colors[] = {Color.rgb(81, 192, 232),
                Color.rgb(196, 129, 224),
                Color.rgb(195, 230, 90),
                Color.rgb(255, 198, 66)};
        int colorIndex = 0;

        for (Entry<Float, String> entry : data.entrySet()) {
            SegmentFormatter sf = new SegmentFormatter(colors[colorIndex], Color.DKGRAY, Color.DKGRAY, Color.DKGRAY);
            sf.getOuterEdgePaint().setStrokeWidth(2);
            sf.getInnerEdgePaint().setStrokeWidth(2);
            sf.getRadialEdgePaint().setStrokeWidth(2);
            sf.getLabelPaint().setColor(Color.DKGRAY);
            sf.getLabelPaint().setTextSize(22);
            sf.getLabelPaint().setTextAlign(Paint.Align.CENTER);
            pie.addSeries(new Segment(entry.getValue().split(":")[0], entry.getKey()), sf);
            colorIndex = ((colorIndex + 1) % colors.length);
            pie.getRenderer(PieRenderer.class).setDonutSize(0, PieRenderer.DonutMode.PERCENT);
        }
        pie.redraw();

        // update list
        ArrayList<String> items = new ArrayList<String>();
        for (Entry<Float, String> entry : data.entrySet()) {
            items.add(entry.getValue());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.stats_list_item,
                R.id.stats_item_label, items);
        lv.setAdapter(adapter);
    }

    public void pickMonth(View v) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        DialogFragment newFragment = new MonthYearPickerDialogFragment(this);
        newFragment.show(ft, "dialog");
    }

    private void setPicker(DatePicker picker) {
        picker.init(mYear, mMonth, 1, onDateSetListener);
        try {
            Field f[] = picker.getClass().getDeclaredFields();
            for (Field field : f) {
                if (field.getName().equals("mDayPicker") || field.getName().equals("mDaySpinner")) {
                    field.setAccessible(true);
                    Object yearPicker = field.get(picker);
                    ((View) yearPicker).setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
        }
    }

    private void updateSpinner(Spinner spinner) {
        try {
            Account item = (Account) spinner.getItemAtPosition(0);
            curAccountId = item.getId();
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                    Account item = (Account) parent.getItemAtPosition(pos);
                    curAccountId = item.getId();


                    plotHandler.removeCallbacks(plotRunnable);
                    plotHandler.post(plotRunnable);

                    pieHandler.removeCallbacks(pieRunnable);
                    pieHandler.post(pieRunnable);

                    accountsSpinner1.setSelection(pos);
                    accountsSpinner2.setSelection(pos);
                }

                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        } catch (Exception e) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_stats, container, false);

        switcher = (ViewSwitcher) v.findViewById(R.id.statsTypeSwitcher);
        lv = (ListView) v.findViewById(R.id.typesList);
        plot = (XYPlot) v.findViewById(R.id.mySimpleXYPlot);
        pie = (PieChart) v.findViewById(R.id.mySimplePieChart);
        updateChartsSettings();

        accountsSpinner1 = (Spinner) v.findViewById(R.id.accountsSpinner);
        accountsSpinner2 = (Spinner) v.findViewById(R.id.accountsSpinner2);

        updateAccountsSpinner(accountsSpinner1);
        updateAccountsSpinner(accountsSpinner2);

        updateSpinner((Spinner) v.findViewById(R.id.accountsSpinner));
        updateSpinner((Spinner) v.findViewById(R.id.accountsSpinner2));

        // create pie
        pieHandler.removeCallbacks(pieRunnable);
        pieHandler.post(pieRunnable);

        DatePicker picker1 = (DatePicker) v.findViewById(R.id.datePicker3);
        DatePicker picker2 = (DatePicker) v.findViewById(R.id.datePicker4);

        setPicker(picker1);
        setPicker(picker2);

        return v;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        mYear = year;
        mMonth = monthOfYear;
        new Handler().post(new Runnable() {
            @Override
            public void run() {
            }
        });

        pieHandler.removeCallbacks(new UpdatePieRunnable());
        pieHandler.post(new UpdatePieRunnable());
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Activity a = getActivity();
            if (a != null)
                a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        }
    }

    private final class UpdatePlotRunnable implements Runnable {
        @Override
        public void run() {
            updatePlot(curAccountId);
        }
    }

    private final class UpdatePieRunnable implements Runnable {

        @Override
        public void run() {
            try {
                drawPie(getPieData(curAccountId));
            } catch (ParseException e) {
            }
        }
    }

    private class ComplexUpdatePieRunnable implements Runnable {

        private long stopTs;
        private long startTs;
        private long currAccountId;

        @Override
        public void run() {
            drawPie(getPieData(currAccountId, startTs, stopTs));
        }

        public void setStopTs(final long stopTs) {
            this.stopTs = stopTs;
        }

        public void setStartTs(final long startTs) {
            this.startTs = startTs;
        }

        public long getStartTs() {
            return startTs;
        }

        public void setCurrAccountId(final long currAccountId) {
            this.currAccountId = currAccountId;
        }

        public long getCurrAccountId() {
            return currAccountId;
        }
    }
}








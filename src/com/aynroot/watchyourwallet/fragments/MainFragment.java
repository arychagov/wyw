package com.aynroot.watchyourwallet.fragments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aynroot.watchyourwallet.*;
import com.aynroot.watchyourwallet.currency.Transaction;
import com.aynroot.watchyourwallet.db.TransactionsData;
import com.aynroot.watchyourwallet.items.EntryAdapter;
import com.aynroot.watchyourwallet.items.EntryTransaction;
import com.aynroot.watchyourwallet.items.Item;
import com.aynroot.watchyourwallet.items.SectionItem;
import net.sqlcipher.database.SQLiteDatabase;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragment;

@SuppressLint("SimpleDateFormat")
public class MainFragment extends SherlockFragment {

    private TransactionsData datasource;
    private ListView lv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        SQLiteDatabase.loadLibs(getActivity());
        View v = inflater.inflate(R.layout.fragment_main, container, false);
        if (v != null) {
            lv = (ListView) v.findViewById(R.id.plannedTransactionsList);

            datasource = new TransactionsData(getActivity());
            updateTransactionsList();

            lv.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position,
                                        long id_item) {

                    EntryAdapter lvAdapter = (EntryAdapter) lv.getAdapter();
                    final Transaction t = ((EntryTransaction) lvAdapter.getItem((int) id_item)).getTransaction();
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                    alertDialogBuilder.setTitle(getResources().getString(R.string.erase_data));
                    alertDialogBuilder
                            .setMessage(getResources().getString(R.string.complete_or_postpone))
                            .setCancelable(true)
                            .setPositiveButton(getResources().getString(R.string.complete), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id_dialog) {
                                    datasource.postponeTransaction(t);
                                    updateTransactionsList();
                                    dialog.cancel();
                                }
                            })
                            .setNegativeButton(getResources().getString(R.string.postpone), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    datasource.completeTransaction(t);
                                    updateTransactionsList();
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
            });
        }

        return v;
    }

    private void updateTransactionsList() {
        List<Transaction> values = datasource.getAllPlannedTransactions();

        ArrayList<Item> items = new ArrayList<Item>();
        items.add(new SectionItem(getResources().getString(R.string.postponed_transactions)));
        String today = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
        for (Transaction t : values) {
            if (t.getDate().equals(today))
                items.add(new SectionItem(getResources().getString(R.string.planned_for_today)));
            items.add(new EntryTransaction(t.getValue().toString() + "\t" + t.getTransactionDescription(getActivity()), t.getDate(), t));
        }

        EntryAdapter adapter = new EntryAdapter(getActivity(), items);
        lv.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateTransactionsList();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Activity a = getActivity();
            if (a != null)
                a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }
}

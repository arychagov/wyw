package com.aynroot.watchyourwallet.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.actionbarsherlock.app.SherlockFragment;
import com.aynroot.watchyourwallet.*;
import com.aynroot.watchyourwallet.activities.ActivityAbout;
import com.aynroot.watchyourwallet.activities.EditIncomeTypesActivity;
import com.aynroot.watchyourwallet.activities.EditSpendingsTypesActivity;
import com.aynroot.watchyourwallet.activities.PasswordSettingsActivity;
import com.aynroot.watchyourwallet.db.SpendingsOrIncomeTypesData;
import com.aynroot.watchyourwallet.db.TransactionsData;
import com.aynroot.watchyourwallet.db.AccountsData;
import com.aynroot.watchyourwallet.db.DatabaseHelper;


public class SettingsFragment extends SherlockFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    public void editSpendingsTypes(View v) {
        Intent intent = new Intent(getActivity(), EditSpendingsTypesActivity.class);
        startActivity(intent);
    }

    public void editIncomeTypes(View v) {
        Intent intent = new Intent(getActivity(), EditIncomeTypesActivity.class);
        startActivity(intent);
    }

    public void editPasswordSettings(View v) {
        Intent intent = new Intent(getActivity(), PasswordSettingsActivity.class);
        startActivityForResult(intent, 1);
    }

    public void eraseData(View v) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(getResources().getString(R.string.erase_data));
        alertDialogBuilder
                .setMessage(getResources().getString(R.string.are_you_sure_message))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        AccountsData ad = new AccountsData(getActivity());
                        ad.deleteTable();

                        SpendingsOrIncomeTypesData soitd = new SpendingsOrIncomeTypesData(getActivity(), DatabaseHelper.SpendingTypesTableInfo.SpendingTypesTableEntry.TABLE_SPENDINGS_TYPES);
                        soitd.deleteTable();
                        soitd = new SpendingsOrIncomeTypesData(getActivity(), DatabaseHelper.IncomeTypesTableInfo.IncomeTypesTableEntry.TABLE_INCOME_TYPES);
                        soitd.deleteTable();

                        TransactionsData td = new TransactionsData(getActivity());
                        td.deleteTables();

                        dialog.cancel();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void about(View v) {
        Intent intent = new Intent(getActivity(), ActivityAbout.class);
        startActivity(intent);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Activity a = getActivity();
            if (a != null)
                a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }
}

package com.aynroot.watchyourwallet.fragments;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.aynroot.watchyourwallet.*;
import com.aynroot.watchyourwallet.activities.CreateTransactionActivity;
import com.aynroot.watchyourwallet.currency.Transaction;
import com.aynroot.watchyourwallet.currency.TransactionType;
import com.aynroot.watchyourwallet.db.TransactionsData;
import com.aynroot.watchyourwallet.db.AccountsData;
import com.aynroot.watchyourwallet.items.EntryAdapter;
import com.aynroot.watchyourwallet.items.EntryTransaction;
import com.aynroot.watchyourwallet.items.Item;
import com.aynroot.watchyourwallet.items.SectionItem;
import net.sqlcipher.database.SQLiteDatabase;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;

@SuppressLint("SimpleDateFormat")
public class TransactionsFragment extends SherlockFragment {

    public final static String TRANSACTION_CREATED_MESSAGE = "com.aynroot.watchyourwallet.TRANSACTION_CREATED_MESSAGE";
    private TransactionsData dataSource = null;
    private ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        SQLiteDatabase.loadLibs(getActivity());
        View v = inflater.inflate(R.layout.fragment_transactions, container, false);
        if (v != null) {
            listView = (ListView) v.findViewById(R.id.transactionsList);
            listView.setLongClickable(true);
            listView.setOnItemLongClickListener(new OnItemLongClickListener() {
                public boolean onItemLongClick(AdapterView<?> parent, View v, int position, long id) {
                    final int itemId = (int) id;
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                    alertDialogBuilder.setTitle(getResources().getString(R.string.delete_transaction));
                    alertDialogBuilder
                            .setMessage(getResources().getString(R.string.are_you_sure_message))
                            .setCancelable(false)
                            .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    deleteRow(itemId);
                                    dialog.cancel();
                                }
                            })
                            .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                    return true;
                }
            });

            dataSource = new TransactionsData(getActivity());
            updateTransactionsList();
        }

        return v;
    }

    private void updateTransactionsList() {
        List<Transaction> values = dataSource.getAllPreviousTransactions();

        ArrayList<Item> items = new ArrayList<Item>();
        DateFormatSymbols dfs = new DateFormatSymbols();
        dfs.setMonths(new String[]{
                getResources().getString(R.string.january),
                getResources().getString(R.string.february),
                getResources().getString(R.string.march),
                getResources().getString(R.string.april),
                getResources().getString(R.string.may),
                getResources().getString(R.string.june),
                getResources().getString(R.string.july),
                getResources().getString(R.string.august),
                getResources().getString(R.string.september),
                getResources().getString(R.string.october),
                getResources().getString(R.string.november),
                getResources().getString(R.string.december)
        });
        SimpleDateFormat monthFmt = new SimpleDateFormat("MMMMM yyyy", dfs);
        Integer curMonth = -1;
        for (Transaction t : values) {
            if (!curMonth.equals(t.getMonth())) {
                curMonth = t.getMonth();
                GregorianCalendar cal = new GregorianCalendar();
                cal.set(Calendar.MONTH, curMonth - 1);
                items.add(new SectionItem(monthFmt.format(cal.getTime())));
            }
            items.add(new EntryTransaction(t.getValue().toString() + " " + t.getCurrency() + ' ' + t.getTransactionDescription(getActivity()), t.getDate() + "\t" + t.getComment(), t));
        }

        EntryAdapter adapter = new EntryAdapter(getActivity(), items);
        listView.setAdapter(adapter);
    }

    public void addRow(View v) {
        Intent intent = new Intent(getActivity(), CreateTransactionActivity.class);
        startActivityForResult(intent, 1);
    }

    public void deleteRow(int id) {
        EntryAdapter lvAdapter = (EntryAdapter) listView.getAdapter();
        Transaction transaction;
        transaction = (Transaction) ((EntryTransaction) lvAdapter.getItem(id)).getTransaction();

        dataSource.deleteTransaction(transaction);

        updateAccountBalance(transaction.getType(), transaction.getFromId(),
                transaction.getToId(), -transaction.getValue());
        updateTransactionsList();
    }

    public void updateAccountBalance(String tTypeStr, int fromId, int toId, float value) {
        TransactionType tType = TransactionType.valueOf(tTypeStr);
        AccountsData accountsDataSource = new AccountsData(getActivity());
        switch (tType) {
            case SPENDING:
                accountsDataSource.updateAccountBalance(fromId, -value);
                break;
            case INCOME:
                accountsDataSource.updateAccountBalance(toId, value);
                break;
            case TRANSFER:
                accountsDataSource.updateAccountBalance(fromId, -value);
                accountsDataSource.updateAccountBalance(toId, value);
                break;
        }
    }


    @SuppressLint("SimpleDateFormat")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null)
            return;
        String transactionContent = data.getStringExtra(TRANSACTION_CREATED_MESSAGE);
        String[] parts = transactionContent.split("\t");
        String comment = parts[0];
        String date = parts[1];
        String tType = parts[2];
        Integer fromId = Integer.valueOf(parts[3]);
        Integer toId = Integer.valueOf(parts[4]);
        Float value = Float.valueOf(parts[5]);

        if (fromId == -1 || toId == -1) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(getResources().getString(R.string.missed_transaction_param_title))
                    .setMessage(getResources().getString(R.string.missed_transaction_param_message))
                    .setCancelable(false)
                    .setNegativeButton(getResources().getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();
            return;
        }

        AccountsData ad = new AccountsData(getActivity());
        Float curBalance = ad.getCurBalance(fromId);
        if (value > curBalance && !TransactionType.INCOME.name().equals(tType)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(getResources().getString(R.string.not_enough_money))
                    .setMessage(getResources().getString(R.string.not_enough_money_full))
                    .setCancelable(false)
                    .setNegativeButton(getResources().getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();
            return;
        } else if (fromId.equals(toId) && TransactionType.TRANSFER.name().equals(tType)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(getResources().getString(R.string.useless_transaction_title))
                    .setMessage(getResources().getString(R.string.useless_transaction_message))
                    .setCancelable(false)
                    .setNegativeButton(getResources().getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();
            return;
        }


        try {
            Transaction t = dataSource.createTransaction(comment, "RUB", value, fromId, toId, date, tType);

            Date today = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            if (Transaction.unixTsFromDate(date) <= Transaction.unixTsFromDate(sdf.format(today))) {
                dataSource.updateTransactionState(t.getType(), t.getId());
                updateAccountBalance(tType, fromId, toId, value);
            } else {
                Toast.makeText(getActivity(), R.string.PostponedTransactionCreated, Toast.LENGTH_LONG).show();
            }
            updateTransactionsList();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (dataSource != null)
            updateTransactionsList();
    }
}
package com.aynroot.watchyourwallet.currency;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.content.Context;
import com.aynroot.watchyourwallet.R;
import com.aynroot.watchyourwallet.db.DatabaseHelper;
import com.aynroot.watchyourwallet.db.SpendingsOrIncomeTypesData;

public class Transaction implements Comparable<Transaction> {
    private long id;
    private String comment;
    private Currency currency;
    private String date;
    private String type;
    private Integer fromId;
    private Integer toId;
    private Float value;
    private Integer done;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getMonth() {
        return Integer.valueOf(date.substring(3, 5));
    }

    @SuppressLint("SimpleDateFormat")
    public static String dateFromUnixTs(long unixTs) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        return sdf.format(new Date(unixTs * 1000));
    }

    @SuppressLint("SimpleDateFormat")
    public static long unixTsFromDate(String dateStr) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        Date date = sdf.parse(dateStr);
        return date.getTime() / 1000;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getFromId() {
        return fromId;
    }

    public void setFromId(int fromId) {
        this.fromId = fromId;
    }

    public int getToId() {
        return toId;
    }

    public void setToId(int toId) {
        this.toId = toId;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public Integer getDone() {
        return done;
    }

    public void setDone(int done) {
        this.done = done;
    }

    @Override
    public String toString() {
        return value.toString() + "; " + date + "; " + type;
    }

    @Override
    public int compareTo(Transaction t) {
        try {
            if (Transaction.unixTsFromDate(date) < Transaction.unixTsFromDate(t.getDate()))
                return -1;
            else if (Transaction.unixTsFromDate(date) == Transaction.unixTsFromDate(t.getDate()))
                return 0;
            else
                return 1;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return 0;
    }

    public String getTransactionDescription(Context context) {
        TransactionType tType = TransactionType.valueOf(type);
        SpendingsOrIncomeTypesData a = new SpendingsOrIncomeTypesData(context, DatabaseHelper.AccountTableInfo.AccountTableEntry.TABLE_ACCOUNTS);
        String result = null;
        switch (tType) {
            case SPENDING:
                SpendingsOrIncomeTypesData s = new SpendingsOrIncomeTypesData(context, DatabaseHelper.SpendingTypesTableInfo.SpendingTypesTableEntry.TABLE_SPENDINGS_TYPES);
                result = s.getNameById(toId) + " (" + context.getResources().getString(R.string.by) + " " + a.getNameById(fromId) + ")";
                break;
            case INCOME:
                SpendingsOrIncomeTypesData i = new SpendingsOrIncomeTypesData(context, DatabaseHelper.IncomeTypesTableInfo.IncomeTypesTableEntry.TABLE_INCOME_TYPES);
                result = i.getNameById(fromId) + " (" + context.getResources().getString(R.string.to) + a.getNameById(toId) + ")";
                break;
            case TRANSFER:
                String from = a.getNameById(fromId);
                String to = a.getNameById(toId);

                result = " " + from + context.getResources().getString(R.string.to) + to;
                break;
        }
        return result;
    }
}

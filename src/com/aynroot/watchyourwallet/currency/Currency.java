package com.aynroot.watchyourwallet.currency;

import java.util.*;

public enum Currency {
    RUB("RUB"),
    USD("USD"),
    EUR("EUR");

    private String tag;

    Currency(final String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }

    public static List<String> getCurrencies() {
        final List<String> result = new ArrayList<String>();
        for (final Currency currency : values()) {
            result.add(currency.tag);
        }
        return result;
    }
}

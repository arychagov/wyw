package com.aynroot.watchyourwallet.currency;

import android.content.Context;
import com.aynroot.watchyourwallet.R;

public enum TransactionType {
    SPENDING,
    INCOME,
    TRANSFER;

    private static Context context;

    public static void setContext(final Context context) {
        TransactionType.context = context;
    }

    public int getPosition() {
        final TransactionType[] values = values();
        for (int i = 0; i < values().length; i++) {
            if (this.equals(values[i])) {
                return i;
            }
        }

        throw new IllegalStateException();
    }

    @Override
    public String toString() {
        if (context != null) {
            switch (this) {
                case SPENDING:
                    return context.getResources().getString(R.string.spending_title);
                case INCOME:
                    return context.getResources().getString(R.string.income_title);
                case TRANSFER:
                    return context.getResources().getString(R.string.transfer_title);
            }
        }

        return super.toString();
    }
}

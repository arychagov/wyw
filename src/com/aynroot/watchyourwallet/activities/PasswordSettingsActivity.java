package com.aynroot.watchyourwallet.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import com.aynroot.watchyourwallet.utils.MD5;
import com.aynroot.watchyourwallet.R;

public class PasswordSettingsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_settings);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.password_settings, menu);
        return true;
    }

    public void Save(View v) {
        // if wrong old -- fail alert

        EditText oldEdit = (EditText) findViewById(R.id.oldEdit);
        EditText newEdit = (EditText) findViewById(R.id.newEdit);
        String oldPswd = oldEdit.getText().toString();
        String newPswd = newEdit.getText().toString();

        SharedPreferences sharedPref = this.getSharedPreferences(
                getString(R.string.preferences_file),
                Context.MODE_PRIVATE);
        String saved_password = sharedPref.getString(getString(R.string.password_key), "");

        MD5 md5 = new MD5();
        if (!saved_password.equals("") && !md5.getHash(oldPswd).equals(saved_password)) {
            //alert
//    		TextView wrngPswd = (TextView) findViewById(R.id.wrong_pswd_text_view);
//    		wrngPswd.setText(getString(R.string.wrong_password));
//    		oldEdit.setText("");
        } else {
            if (saved_password.equals("")) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(getString(R.string.password_key), md5.getHash(newPswd));
                editor.commit();
            }
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
        }
    }

}

package com.aynroot.watchyourwallet.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.aynroot.watchyourwallet.*;
import com.aynroot.watchyourwallet.fragments.*;

public class NavigationActivity extends SherlockFragmentActivity implements
        ActionBar.TabListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
     * will keep every loaded fragment in memory. If this becomes too memory
     * intensive, it may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    private AccountsFragment accountsFragment;
    private TransactionsFragment transactionsFragment;
    private StatsFragment statsFragment;
    private SettingsFragment settingsFragment;

    public enum TabType {
        ACCOUNTS, TRANSACTIONS, MAIN, STATS, SETTINGS
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);

        mSectionsPagerAdapter = new SectionsPagerAdapter(
                getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mViewPager
                .setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                        actionBar.setSelectedNavigationItem(position);
                    }
                });

        actionBar.addTab(actionBar.newTab()
                .setTabListener(this)
                .setIcon(R.drawable.ic_tab_accounts));
        actionBar.addTab(actionBar.newTab()
                .setTabListener(this)
                .setIcon(R.drawable.ic_tab_transactions));
        actionBar.addTab(actionBar.newTab()
                .setTabListener(this)
                .setIcon(R.drawable.ic_tab_main));
        actionBar.addTab(actionBar.newTab()
                .setTabListener(this)
                .setIcon(R.drawable.ic_tab_stats));
        actionBar.addTab(actionBar.newTab()
                .setTabListener(this)
                .setIcon(R.drawable.ic_tab_settings));
        mViewPager.setCurrentItem(TabType.MAIN.ordinal());
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab,
                              FragmentTransaction fragmentTransaction) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab,
                                FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab,
                                FragmentTransaction fragmentTransaction) {
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    accountsFragment = new AccountsFragment();
                    return accountsFragment;
                case 1:
                    transactionsFragment = new TransactionsFragment();
                    return transactionsFragment;
                case 2:
                    return new MainFragment();
                case 3:
                    statsFragment = new StatsFragment();
                    return statsFragment;
                case 4:
                    settingsFragment = new SettingsFragment();
                    return settingsFragment;
            }
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Accounts";
                case 1:
                    return "Transactions";
                case 2:
                    return "Main";
                case 3:
                    return "Stats";
                case 4:
                    return "Settings";
            }
            return null;
        }

        @Override
        public int getCount() {
            return 5;
        }
    }

	/* Accounts methods */

    public void AccountsAddRow(View v) {
        accountsFragment.addRow(v);
    }
	
	/* Transactions methods */

    public void TransactionsAddRow(View v) {
        transactionsFragment.addRow(v);
    }
	
	/* Settings methods */

    public void SettingsEditSpendingsTypes(View v) {
        settingsFragment.editSpendingsTypes(v);
    }

    public void SettingsEditIncomeTypes(View v) {
        settingsFragment.editIncomeTypes(v);
    }

    public void SettingsEditPasswordSettings(View v) {
        settingsFragment.editPasswordSettings(v);
    }

    public void SettingsEraseData(View v) {
        settingsFragment.eraseData(v);
    }

    public void SettingsAbout(View v) {
        settingsFragment.about(v);
    }
	
	/* Stats methods */

    public void StatsSwitchLayout(View v) {
        statsFragment.switchLayout(v);
    }

    public void StatsPickMonth(View v) {
        statsFragment.pickMonth(v);
    }
}

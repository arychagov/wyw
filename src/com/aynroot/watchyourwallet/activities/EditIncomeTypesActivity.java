package com.aynroot.watchyourwallet.activities;

import java.util.List;

import com.aynroot.watchyourwallet.db.DatabaseHelper;
import com.aynroot.watchyourwallet.R;
import com.aynroot.watchyourwallet.currency.SpendingsOrIncomeType;
import com.aynroot.watchyourwallet.db.SpendingsOrIncomeTypesData;
import net.sqlcipher.database.SQLiteDatabase;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemLongClickListener;

public class EditIncomeTypesActivity extends Activity {
    public final static String INCOME_TYPE_CREATED_MESSAGE = "com.aynroot.watchyourwallet.SPENDINGS_TYPE_CREATED_MESSAGE";
    private ArrayAdapter<SpendingsOrIncomeType> lvAdapter;
    private SpendingsOrIncomeTypesData datasource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_income_types);

        SQLiteDatabase.loadLibs(this);
        ListView lv = (ListView) findViewById(R.id.incomeTypesList);

        final Activity activityContext = this;
        lv.setLongClickable(true);
        lv.setOnItemLongClickListener(new OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> parent, View v, int position, long id) {
                final int itemId = (int) id;
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activityContext);
                alertDialogBuilder.setTitle(getResources().getString(R.string.delete_account_title));
                alertDialogBuilder
                        .setMessage(getResources().getString(R.string.are_you_sure_message))
                        .setCancelable(true)
                        .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                deleteRow(itemId);
                                dialog.cancel();
                            }
                        })
                        .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return true;
            }
        });

        datasource = new SpendingsOrIncomeTypesData(this, DatabaseHelper.IncomeTypesTableInfo.IncomeTypesTableEntry.TABLE_INCOME_TYPES);
        List<SpendingsOrIncomeType> values = datasource.getAllSpendingsOrIncomeTypes();

        lvAdapter = new ArrayAdapter<SpendingsOrIncomeType>(this, android.R.layout.simple_list_item_1, values);
        lv.setAdapter(lvAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.edit_income_types, menu);
        return true;
    }

    public void addRow(View v) {
        Intent intent = new Intent(this, CreateIncomeTypeActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null)
            return;
        String incomeContent = data.getStringExtra(INCOME_TYPE_CREATED_MESSAGE);
        String[] parts = incomeContent.split("\t");

        if (parts[0].equals("")) {
            Toast toast = Toast.makeText(this, getResources().getString(R.string.type_not_created), Toast.LENGTH_SHORT);
            toast.show();
            return;
        }

        SpendingsOrIncomeType incomeType = datasource.createSpendingsOrIncomeType(parts[0]);

        lvAdapter.add(incomeType);
        lvAdapter.notifyDataSetChanged();
    }

    public void deleteRow(int itemId) {
        if (lvAdapter.getCount() > 0) {
            SpendingsOrIncomeType spending = lvAdapter.getItem(itemId);

            datasource.deleteSpendingsOrIncomeType(spending);

            lvAdapter.remove(spending);
            lvAdapter.notifyDataSetChanged();
        }
    }
}

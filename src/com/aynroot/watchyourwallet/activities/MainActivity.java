package com.aynroot.watchyourwallet.activities;

import android.content.Intent;
import android.os.Bundle;

import com.actionbarsherlock.app.SherlockActivity;
import com.aynroot.watchyourwallet.R;


public class MainActivity extends SherlockActivity {
    public static boolean isLoggedIn = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = null;
        if (!isLoggedIn) {
            intent = new Intent(this, LogInActivity.class);
        } else {
            intent = new Intent(this, NavigationActivity.class);
        }
        startActivity(intent);
        finish();
    }

}

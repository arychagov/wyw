package com.aynroot.watchyourwallet.activities;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import com.aynroot.watchyourwallet.R;

public class CreateIncomeTypeActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_income_type);
        findViewById(R.id.createButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                createIncomeType();
            }
        });
    }

    public void createIncomeType() {
        final String name = ((EditText) findViewById(R.id.nameEdit)).getText().toString();
        // add icon info
        final Intent intent = new Intent().putExtra(EditIncomeTypesActivity.INCOME_TYPE_CREATED_MESSAGE, name);
        setResult(RESULT_OK, intent);
        finish();
    }
}

package com.aynroot.watchyourwallet.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.aynroot.watchyourwallet.utils.MD5;
import com.aynroot.watchyourwallet.R;

public class LogInActivity extends SherlockActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        //just for test
        Intent intent = new Intent(this, MainActivity.class);
        MainActivity.isLoggedIn = true;
        startActivity(intent);
        finish();
    }

    public void logIn(View view) {
        EditText pswdEdit = (EditText) findViewById(R.id.password_edit);
        String password = pswdEdit.getText().toString();

        SharedPreferences sharedPref = this.getSharedPreferences(
                getString(R.string.preferences_file),
                Context.MODE_PRIVATE);
        String saved_password = sharedPref.getString(getString(R.string.password_key), "");

        MD5 md5 = new MD5();
        if (!saved_password.equals("") && !md5.getHash(password).equals(saved_password)) {
            TextView wrngPswd = (TextView) findViewById(R.id.wrong_pswd_text_view);
            wrngPswd.setText(getString(R.string.wrong_password));
            pswdEdit.setText("");
        } else {
            if (saved_password.equals("")) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(getString(R.string.password_key), md5.getHash(password));
                editor.commit();
            }
            Intent intent = new Intent(this, MainActivity.class);
            MainActivity.isLoggedIn = true;
            startActivity(intent);
            finish();
        }
    }
}

package com.aynroot.watchyourwallet.activities;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import com.aynroot.watchyourwallet.R;

public class CreateSpendingsTypeActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_spending_type);
        findViewById(R.id.createButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                createSpendingType();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.create_spendings_type, menu);
        return true;
    }

    public void createSpendingType() {
        String name = ((EditText) findViewById(R.id.nameEdit)).getText().toString();
        // add icon info
        final Intent intent = new Intent().putExtra(EditSpendingsTypesActivity.SPENDINGS_TYPE_CREATED_MESSAGE, name);
        setResult(RESULT_OK, intent);
        finish();
    }
}

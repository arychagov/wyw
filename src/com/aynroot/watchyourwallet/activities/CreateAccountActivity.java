package com.aynroot.watchyourwallet.activities;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import com.aynroot.watchyourwallet.fragments.AccountsFragment;
import com.aynroot.watchyourwallet.currency.Currency;
import com.aynroot.watchyourwallet.R;

public class CreateAccountActivity extends Activity {

    private Spinner currencySpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        initCurrencySpinner();
    }

    // add items into spinner dynamically
    public void initCurrencySpinner() {
        currencySpinner = (Spinner) findViewById(R.id.currencySpinner);
        final List<String> currencyList = Currency.getCurrencies();
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, currencyList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        currencySpinner.setAdapter(dataAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.create_transaction, menu);
        return true;
    }

    public void createTransaction(View v) {
        try {
            final String name = ((EditText) findViewById(R.id.nameEdit)).getText().toString();
            final String currency = currencySpinner.getSelectedItem().toString();
            String balance = ((EditText) findViewById(R.id.balanceEdit)).getText().toString();

            if (name.equals("")) {
                Toast.makeText(this, getResources().getString(R.string.no_name), Toast.LENGTH_SHORT).show();
                return;
            }

            if (balance.equals("")) {
                balance = "0";
            }

            final Intent intent = new Intent().putExtra(AccountsFragment.ACCOUNT_CREATED_MESSAGE, name + "\t" + currency + "\t" + balance);
            setResult(RESULT_OK, intent);
            finish();
        } catch (NullPointerException e) {
            Log.e(CreateAccountActivity.class.getSimpleName(), "NPE Happened", e);
        }
    }

}

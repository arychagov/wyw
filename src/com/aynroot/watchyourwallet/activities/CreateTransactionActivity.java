package com.aynroot.watchyourwallet.activities;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.List;

import android.app.ActionBar;
import android.os.Build;
import com.aynroot.watchyourwallet.*;
import com.aynroot.watchyourwallet.currency.Account;
import com.aynroot.watchyourwallet.currency.SpendingsOrIncomeType;
import com.aynroot.watchyourwallet.db.SpendingsOrIncomeTypesData;
import com.aynroot.watchyourwallet.currency.TransactionType;
import com.aynroot.watchyourwallet.db.AccountsData;
import com.aynroot.watchyourwallet.db.DatabaseHelper;
import com.aynroot.watchyourwallet.dialogs.CustomDatePickerDialog;
import com.aynroot.watchyourwallet.fragments.TransactionsFragment;
import com.aynroot.watchyourwallet.utils.ArithmeticalOperations;
import net.sqlcipher.database.SQLiteDatabase;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;

public class CreateTransactionActivity extends SherlockActivity {
    public static String dateChangedMessage = "com.aynroot.watchyourwallet.DATE_CHANGED_MESSAGE";
    private final int DATE_DIALOG_ID = 1;

    private TextView valueText;
    private TextView dateText;
    private Button delEqualButton;

    private Float value = 0.f;
    private boolean isOperation = false;
    private ArithmeticalOperations rememberedOperation;
    private Float rememberedValue = null;

    private static final DecimalFormat[] DECIMAL_FORMATS = new DecimalFormat[]{
            new DecimalFormat(""), new DecimalFormat("#.#"), new DecimalFormat("#.##")
    };

    private int curDecimalMode = 0;

    private ArrayAdapter<Account> accountsAdapter;
    private ArrayAdapter<SpendingsOrIncomeType> spendingsOrIncomeTypeAdapter;

    private TransactionType curTransactionType = TransactionType.SPENDING;


    private int curDay;
    private int curMonth;
    private int curYear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_transaction);

        TransactionType.setContext(this);

        valueText = (TextView) findViewById(R.id.valueTransactionText);
        delEqualButton = (Button) findViewById(R.id.btnC);
        updateValue();

        SQLiteDatabase.loadLibs(this);
        accountsAdapter = updateAccountsSpinner((Spinner) findViewById(R.id.fromSpinner));
        spendingsOrIncomeTypeAdapter = updateSpendingsOrIncomeTypesSpinner((Spinner) findViewById(R.id.toSpinner), DatabaseHelper.SpendingTypesTableInfo.SpendingTypesTableEntry.TABLE_SPENDINGS_TYPES);
        setTitle(R.string.spending_title);

        dateText = (TextView) findViewById(R.id.dateText);
        final Calendar cal = Calendar.getInstance();
        curDay = cal.get(Calendar.DAY_OF_MONTH);
        curMonth = cal.get(Calendar.MONTH);
        curYear = cal.get(Calendar.YEAR);
        dateText.setText(getDateRepresentation(curDay, curMonth, curYear));


        findViewById(R.id.directionButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                changeTransactionType();
                if (Build.VERSION.SDK_INT >= 11) {
                    getSupportActionBar().setSelectedNavigationItem(curTransactionType.getPosition());
                } else {
                    getSupportActionBar().setTitle(curTransactionType.toString());
                }
            }
        });

        if (Build.VERSION.SDK_INT >= 11) {
            ArrayAdapter<TransactionType> adapter = new ArrayAdapter<TransactionType>(this, android.R.layout.simple_spinner_item, TransactionType.values());

            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
            getSupportActionBar().setListNavigationCallbacks(adapter, new com.actionbarsherlock.app.ActionBar.OnNavigationListener() {
                @Override
                public boolean onNavigationItemSelected(final int itemPosition, final long itemId) {
                    curTransactionType = TransactionType.values()[itemPosition];
                    applyTransactionType();
                    return false;
                }
            });
        } else {
            getSupportActionBar().setTitle(curTransactionType.toString());
        }
    }

    //TODO: refactor to SimpleDateFormat ??
    public String getDateRepresentation(int day, int month, int year) {
        String dayString = day < 10 ? "0" + day : "" + day;
        String monthString = (month + 1) < 10 ? "0" + (month + 1) : "" + (month + 1);
        String yearString = "" + year;
        return dayString + "." + monthString + "." + yearString;
    }

    // add items into spinner dynamically
    public ArrayAdapter<Account> updateAccountsSpinner(Spinner spinner) {
        AccountsData accountsData = new AccountsData(getBaseContext());
        List<Account> accountsList = accountsData.getAllAccounts();

        ArrayAdapter<Account> dataAdapter = new ArrayAdapter<Account>(this,
                android.R.layout.simple_spinner_item,
                accountsList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        return dataAdapter;
    }

    public ArrayAdapter<SpendingsOrIncomeType> updateSpendingsOrIncomeTypesSpinner(Spinner spinner, String table) {
        SpendingsOrIncomeTypesData spendingsTypesData = new SpendingsOrIncomeTypesData(getBaseContext(),
                table);
        List<SpendingsOrIncomeType> spendingsTypesList = spendingsTypesData.getAllSpendingsOrIncomeTypes();

        ArrayAdapter<SpendingsOrIncomeType> dataAdapter = new ArrayAdapter<SpendingsOrIncomeType>(this,
                android.R.layout.simple_spinner_item,
                spendingsTypesList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        return dataAdapter;
    }

    public void applyTransactionType() {
        switch (curTransactionType) {
            case INCOME:
                spendingsOrIncomeTypeAdapter = updateSpendingsOrIncomeTypesSpinner((Spinner) findViewById(R.id.fromSpinner),
                        DatabaseHelper.IncomeTypesTableInfo.IncomeTypesTableEntry.TABLE_INCOME_TYPES);
                accountsAdapter = updateAccountsSpinner((Spinner) findViewById(R.id.toSpinner));
                break;
            case TRANSFER:
                accountsAdapter = updateAccountsSpinner((Spinner) findViewById(R.id.fromSpinner));
                accountsAdapter = updateAccountsSpinner((Spinner) findViewById(R.id.toSpinner));
                break;
            case SPENDING:
                accountsAdapter = updateAccountsSpinner((Spinner) findViewById(R.id.fromSpinner));
                spendingsOrIncomeTypeAdapter = updateSpendingsOrIncomeTypesSpinner((Spinner) findViewById(R.id.toSpinner),
                        DatabaseHelper.SpendingTypesTableInfo.SpendingTypesTableEntry.TABLE_SPENDINGS_TYPES);
                break;
        }
    }

    public void changeTransactionType() {
        switch (curTransactionType) {
            case SPENDING:
                curTransactionType = TransactionType.INCOME;
                break;
            case INCOME:
                curTransactionType = TransactionType.TRANSFER;
                break;
            case TRANSFER:
                curTransactionType = TransactionType.SPENDING;
                break;
        }

        applyTransactionType();
    }

    private void modifyTitle() {
        setTitle(curTransactionType.toString());
    }

    @SuppressWarnings("deprecation")
    public void changeDate(View v) {
        showDialog(DATE_DIALOG_ID);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                return new CustomDatePickerDialog(this, datePickerListener, curYear, curMonth, curDay);
        }
        return null;
    }

    private CustomDatePickerDialog.OnDateSetListener datePickerListener = new CustomDatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            curYear = selectedYear;
            curMonth = selectedMonth;
            curDay = selectedDay;

            dateText.setText(getDateRepresentation(curDay, curMonth, curYear));
        }
    };

    public void createTransaction(View v) {

        finishOperation();
        updateValue();

        if (value == 0 && curTransactionType != TransactionType.INCOME) {
            Toast.makeText(this, getResources().getString(R.string.ZeroTransaction), Toast.LENGTH_SHORT).show();
            return;
        } else if (value < 0) {
            Toast.makeText(this, R.string.NegativeTransaction, Toast.LENGTH_SHORT).show();
            return;
        }

        String comment = ((EditText) findViewById(R.id.commentTransactionEdit)).getText().toString();
        String valueString = value.toString();

        String date = ((TextView) findViewById(R.id.dateText)).getText().toString();

        Intent intent = new Intent();
        StringBuilder sb = new StringBuilder();

        sb.append(comment);
        sb.append("\t");
        sb.append(date);
        sb.append("\t");
        sb.append(curTransactionType.name());
        sb.append("\t");
        int fromPosition = ((Spinner) findViewById(R.id.fromSpinner)).getSelectedItemPosition();
        int toPosition = ((Spinner) findViewById(R.id.toSpinner)).getSelectedItemPosition();

        long fromId = -1, toId = -1;


        if (fromPosition != -1 && toPosition != -1) {
            switch (curTransactionType) {
                case SPENDING:
                    fromId = accountsAdapter.getItem(fromPosition).getId();
                    toId = spendingsOrIncomeTypeAdapter.getItem(toPosition).getId();
                    break;
                case INCOME:
                    fromId = spendingsOrIncomeTypeAdapter.getItem(fromPosition).getId();
                    toId = accountsAdapter.getItem(toPosition).getId();
                    break;
                case TRANSFER:
                    fromId = accountsAdapter.getItem(fromPosition).getId();
                    toId = accountsAdapter.getItem(toPosition).getId();
                    break;
            }
        }
        sb.append(String.valueOf(fromId));
        sb.append("\t");
        sb.append(String.valueOf(toId));
        sb.append("\t");
        sb.append(valueString);
        // final format: comment, date, transaction_type, from_id, to_id, value
        intent.putExtra(TransactionsFragment.TRANSACTION_CREATED_MESSAGE, sb.toString());
        setResult(RESULT_OK, intent);
        finish();
    }

    /**
     * ****************************  calculator operations  ******************************
     */

    public void updateDigitMode() {
        if (value == Math.ceil(value)) {
            curDecimalMode = 0;
        } else if (value * 10 == Math.ceil(value * 10)) {
            curDecimalMode = 1;
        } else {
            curDecimalMode = 2;
        }
    }

    public void updateValue() {
        valueText.setText(DECIMAL_FORMATS[curDecimalMode > 2 ? 2 : curDecimalMode].format(value));
    }

    public void prepareForOperation() {
        rememberedValue = value;
        value = 0.f;
        delEqualButton.setText("=");
        isOperation = true;
        curDecimalMode = 0;
    }

    public void finishOperation() {
        if (!isOperation)
            return;
        if (rememberedValue != null) {
            switch (rememberedOperation) {
                case PLUS:
                    value += rememberedValue;
                    break;
                case MINUS:
                    value = rememberedValue - value;
                    break;
                case MULTIPLY:
                    value *= rememberedValue;
                    break;
                case FACTOR:
                    value = rememberedValue / value;
                    break;
            }
        }
        rememberedValue = null;
        delEqualButton.setText("C");
        isOperation = false;
    }

    public void ClickDigit(View v) {
        int num = Integer.parseInt(v.getTag().toString());
        if (curDecimalMode == 0)
            value = value * 10 + num;
        else if (curDecimalMode == 1) {
            value = value + num / 10.f;
            curDecimalMode = 2;
        } else if (curDecimalMode == 2) {
            value = value + num / 100.f;
            curDecimalMode = 3;
        } else
            return;
        updateValue();
    }

    public void ClickComma(View v) {
        if (curDecimalMode != 0)
            return;
        curDecimalMode = 1;
        valueText.setText((valueText.getText().toString() + ","));
    }

    public void ClickDelEqual(View v) {
        if (isOperation) {
            finishOperation();
            updateDigitMode();
            updateValue();
        } else {
            value = 0.f;
            rememberedValue = null;
            updateDigitMode();
            updateValue();
        }
    }

    public void ClickPlus(View v) {
        prepareForOperation();
        rememberedOperation = ArithmeticalOperations.PLUS;
    }

    public void ClickMinus(View v) {
        prepareForOperation();
        rememberedOperation = ArithmeticalOperations.MINUS;
    }

    public void ClickMult(View v) {
        prepareForOperation();
        rememberedOperation = ArithmeticalOperations.MULTIPLY;
    }

    public void ClickDivide(View v) {
        prepareForOperation();
        rememberedOperation = ArithmeticalOperations.FACTOR;
    }

}
